defmodule SimpleBudgetWeb.BudgetCategoryControllerTest do
  use SimpleBudgetWeb.ConnCase

  alias SimpleBudget.BudgetCategories

  @create_attrs %{amount: 120.5, desc: "some desc", name: "some name", soft_delete: true}
  @update_attrs %{amount: 456.7, desc: "some updated desc", name: "some updated name", soft_delete: false}
  @invalid_attrs %{amount: nil, desc: nil, name: nil, soft_delete: nil}

  def fixture(:budget_category) do
    {:ok, budget_category} = BudgetCategories.create_budget_category(@create_attrs)
    budget_category
  end

  describe "index" do
    test "lists all budget_categories", %{conn: conn} do
      conn = get conn, budget_category_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Budget categories"
    end
  end

  describe "new budget_category" do
    test "renders form", %{conn: conn} do
      conn = get conn, budget_category_path(conn, :new)
      assert html_response(conn, 200) =~ "New Budget category"
    end
  end

  describe "create budget_category" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, budget_category_path(conn, :create), budget_category: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == budget_category_path(conn, :show, id)

      conn = get conn, budget_category_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Budget category"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, budget_category_path(conn, :create), budget_category: @invalid_attrs
      assert html_response(conn, 200) =~ "New Budget category"
    end
  end

  describe "edit budget_category" do
    setup [:create_budget_category]

    test "renders form for editing chosen budget_category", %{conn: conn, budget_category: budget_category} do
      conn = get conn, budget_category_path(conn, :edit, budget_category)
      assert html_response(conn, 200) =~ "Edit Budget category"
    end
  end

  describe "update budget_category" do
    setup [:create_budget_category]

    test "redirects when data is valid", %{conn: conn, budget_category: budget_category} do
      conn = put conn, budget_category_path(conn, :update, budget_category), budget_category: @update_attrs
      assert redirected_to(conn) == budget_category_path(conn, :show, budget_category)

      conn = get conn, budget_category_path(conn, :show, budget_category)
      assert html_response(conn, 200) =~ "some updated desc"
    end

    test "renders errors when data is invalid", %{conn: conn, budget_category: budget_category} do
      conn = put conn, budget_category_path(conn, :update, budget_category), budget_category: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Budget category"
    end
  end

  describe "delete budget_category" do
    setup [:create_budget_category]

    test "deletes chosen budget_category", %{conn: conn, budget_category: budget_category} do
      conn = delete conn, budget_category_path(conn, :delete, budget_category)
      assert redirected_to(conn) == budget_category_path(conn, :index)
      assert_error_sent 404, fn ->
        get conn, budget_category_path(conn, :show, budget_category)
      end
    end
  end

  defp create_budget_category(_) do
    budget_category = fixture(:budget_category)
    {:ok, budget_category: budget_category}
  end
end
