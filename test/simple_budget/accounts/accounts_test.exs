defmodule SimpleBudget.AccountsTest do
  use SimpleBudget.DataCase

  alias SimpleBudget.Accounts

  describe "accounts" do
    alias SimpleBudget.Accounts.Account

    @valid_attrs %{desc: "some desc", name: "some name", number: "some number", soft_delete: true, total_amount: 120.5}
    @update_attrs %{desc: "some updated desc", name: "some updated name", number: "some updated number", soft_delete: false, total_amount: 456.7}
    @invalid_attrs %{desc: nil, name: nil, number: nil, soft_delete: nil, total_amount: nil}

    def account_fixture(attrs \\ %{}) do
      {:ok, account} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_account()

      account
    end

    test "list_accounts/0 returns all accounts" do
      account = account_fixture()
      assert Accounts.list_accounts() == [account]
    end

    test "get_account!/1 returns the account with given id" do
      account = account_fixture()
      assert Accounts.get_account!(account.id) == account
    end

    test "create_account/1 with valid data creates a account" do
      assert {:ok, %Account{} = account} = Accounts.create_account(@valid_attrs)
      assert account.desc == "some desc"
      assert account.name == "some name"
      assert account.number == "some number"
      assert account.soft_delete == true
      assert account.total_amount == 120.5
    end

    test "create_account/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_account(@invalid_attrs)
    end

    test "update_account/2 with valid data updates the account" do
      account = account_fixture()
      assert {:ok, account} = Accounts.update_account(account, @update_attrs)
      assert %Account{} = account
      assert account.desc == "some updated desc"
      assert account.name == "some updated name"
      assert account.number == "some updated number"
      assert account.soft_delete == false
      assert account.total_amount == 456.7
    end

    test "update_account/2 with invalid data returns error changeset" do
      account = account_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_account(account, @invalid_attrs)
      assert account == Accounts.get_account!(account.id)
    end

    test "delete_account/1 deletes the account" do
      account = account_fixture()
      assert {:ok, %Account{}} = Accounts.delete_account(account)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_account!(account.id) end
    end

    test "change_account/1 returns a account changeset" do
      account = account_fixture()
      assert %Ecto.Changeset{} = Accounts.change_account(account)
    end
  end
end
