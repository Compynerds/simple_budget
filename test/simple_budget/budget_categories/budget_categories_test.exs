defmodule SimpleBudget.BudgetCategoriesTest do
  use SimpleBudget.DataCase

  alias SimpleBudget.BudgetCategories

  describe "budget_categories" do
    alias SimpleBudget.BudgetCategories.BudgetCategory

    @valid_attrs %{amount: 120.5, desc: "some desc", name: "some name", soft_delete: true}
    @update_attrs %{amount: 456.7, desc: "some updated desc", name: "some updated name", soft_delete: false}
    @invalid_attrs %{amount: nil, desc: nil, name: nil, soft_delete: nil}

    def budget_category_fixture(attrs \\ %{}) do
      {:ok, budget_category} =
        attrs
        |> Enum.into(@valid_attrs)
        |> BudgetCategories.create_budget_category()

      budget_category
    end

    test "list_budget_categories/0 returns all budget_categories" do
      budget_category = budget_category_fixture()
      assert BudgetCategories.list_budget_categories() == [budget_category]
    end

    test "get_budget_category!/1 returns the budget_category with given id" do
      budget_category = budget_category_fixture()
      assert BudgetCategories.get_budget_category!(budget_category.id) == budget_category
    end

    test "create_budget_category/1 with valid data creates a budget_category" do
      assert {:ok, %BudgetCategory{} = budget_category} = BudgetCategories.create_budget_category(@valid_attrs)
      assert budget_category.amount == 120.5
      assert budget_category.desc == "some desc"
      assert budget_category.name == "some name"
      assert budget_category.soft_delete == true
    end

    test "create_budget_category/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = BudgetCategories.create_budget_category(@invalid_attrs)
    end

    test "update_budget_category/2 with valid data updates the budget_category" do
      budget_category = budget_category_fixture()
      assert {:ok, budget_category} = BudgetCategories.update_budget_category(budget_category, @update_attrs)
      assert %BudgetCategory{} = budget_category
      assert budget_category.amount == 456.7
      assert budget_category.desc == "some updated desc"
      assert budget_category.name == "some updated name"
      assert budget_category.soft_delete == false
    end

    test "update_budget_category/2 with invalid data returns error changeset" do
      budget_category = budget_category_fixture()
      assert {:error, %Ecto.Changeset{}} = BudgetCategories.update_budget_category(budget_category, @invalid_attrs)
      assert budget_category == BudgetCategories.get_budget_category!(budget_category.id)
    end

    test "delete_budget_category/1 deletes the budget_category" do
      budget_category = budget_category_fixture()
      assert {:ok, %BudgetCategory{}} = BudgetCategories.delete_budget_category(budget_category)
      assert_raise Ecto.NoResultsError, fn -> BudgetCategories.get_budget_category!(budget_category.id) end
    end

    test "change_budget_category/1 returns a budget_category changeset" do
      budget_category = budget_category_fixture()
      assert %Ecto.Changeset{} = BudgetCategories.change_budget_category(budget_category)
    end
  end
end
