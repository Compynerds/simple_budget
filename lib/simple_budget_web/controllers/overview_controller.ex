defmodule SimpleBudgetWeb.OverviewController do
  use SimpleBudgetWeb, :controller
  require IEx

  alias SimpleBudget.Transactions
  alias SimpleBudget.Transactions.Transaction
  alias SimpleBudget.BudgetCategories
  alias SimpleBudget.BudgetCategories.BudgetCategory

  def index(conn, _params) do
    transactions = sum_transactions(Transactions.list_transactions(), [])
    categories = unique_category_info(BudgetCategories.list_budget_categories(), [])
    render(conn, "index.html", transactions: transactions, categories: categories)
  end

  def sum_transactions([head | tail], accumulator) do
      sum_transactions(tail, [head.category_id] ++ accumulator)
  end

  def sum_transactions([], accumulator) do
    accumulator
  end

  def unique_category_info([head | tail], accumulator) do
    unique_category_info(tail, [%{"name" => head.name, "id" => head.id}] ++ accumulator)
  end

  def unique_category_info([], accumulator) do
    accumulator
  end
end
