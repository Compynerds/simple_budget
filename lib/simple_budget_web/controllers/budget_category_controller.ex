defmodule SimpleBudgetWeb.BudgetCategoryController do
  use SimpleBudgetWeb, :controller

  alias SimpleBudget.BudgetCategories
  alias SimpleBudget.BudgetCategories.BudgetCategory

  def index(conn, _params) do
    budget_categories = BudgetCategories.list_budget_categories()
    render(conn, "index.html", budget_categories: budget_categories)
  end

  def new(conn, _params) do
    changeset = BudgetCategories.change_budget_category(%BudgetCategory{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"budget_category" => budget_category_params}) do
    case BudgetCategories.create_budget_category(budget_category_params) do
      {:ok, budget_category} ->
        conn
        |> put_flash(:info, "Budget category created successfully.")
        |> redirect(to: budget_category_path(conn, :show, budget_category))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    budget_category = BudgetCategories.get_budget_category!(id)
    render(conn, "show.html", budget_category: budget_category)
  end

  def edit(conn, %{"id" => id}) do
    budget_category = BudgetCategories.get_budget_category!(id)
    changeset = BudgetCategories.change_budget_category(budget_category)
    render(conn, "edit.html", budget_category: budget_category, changeset: changeset)
  end

  def update(conn, %{"id" => id, "budget_category" => budget_category_params}) do
    budget_category = BudgetCategories.get_budget_category!(id)

    case BudgetCategories.update_budget_category(budget_category, budget_category_params) do
      {:ok, budget_category} ->
        conn
        |> put_flash(:info, "Budget category updated successfully.")
        |> redirect(to: budget_category_path(conn, :show, budget_category))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", budget_category: budget_category, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    budget_category = BudgetCategories.get_budget_category!(id)
    {:ok, _budget_category} = BudgetCategories.delete_budget_category(budget_category)

    conn
    |> put_flash(:info, "Budget category deleted successfully.")
    |> redirect(to: budget_category_path(conn, :index))
  end
end
