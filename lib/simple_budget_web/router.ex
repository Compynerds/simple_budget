defmodule SimpleBudgetWeb.Router do
  use SimpleBudgetWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", SimpleBudgetWeb do
    pipe_through :browser # Use the default browser stack

    resources "/accounts", AccountController
    resources "/transactions", TransactionController
    resources "/budget_categories", BudgetCategoryController
    get "/overview", OverviewController, :index
  end


  # Other scopes may use custom stacks.
  # scope "/api", SimpleBudgetWeb do
  #   pipe_through :api
  # end
end
