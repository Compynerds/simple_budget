defmodule SimpleBudget.Transactions do
  require IEx

  @moduledoc """
  The Transactions context.
  """

  import Ecto.Query, warn: false
  alias SimpleBudget.Repo

  alias SimpleBudget.Transactions.Transaction

  @doc """
  Returns the list of transactions.

  ## Examples

      iex> list_transactions()
      [%Transaction{}, ...]

  """
  def list_transactions do
    Repo.all(Transaction)
  end

  @doc """
  Gets a single transaction.

  Raises `Ecto.NoResultsError` if the Transaction does not exist.

  ## Examples

      iex> get_transaction!(123)
      %Transaction{}

      iex> get_transaction!(456)
      ** (Ecto.NoResultsError)

  """
  def get_transaction!(id), do: Repo.get!(Transaction, id)

  @doc """
  Creates a transaction.

  ## Examples

      iex> create_transaction(%{field: value})
      {:ok, %Transaction{}}

      iex> create_transaction(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_transaction(attrs \\ %{}) do
    # TODO: couldn't get the CSV working 100%
#    if attrs["csv_file"]!= nil && attrs["csv_file"].path != "" do
#      attrs["csv_file"].path
#      |> File.stream!()
#      |> CSV.decode(delimiter: ",", headers: false)
#      |> Enum.map( fn row ->
#        {:ok, result} = row
#          hold = create_transaction_from_csv(%{name: Enum.at(result, 0), amount: Enum.at(result, 1),
#            account_id: Enum.at(result, 2), category_id: Enum.at(result, 3),
#            desc: Enum.at(result, 4)})
#          IEx.pry
#        {:error, result} = row
#          result
#      end)
#    else
      %Transaction{}
      |> Transaction.changeset(attrs)
      |> Repo.insert()
#    end
  end


  @doc """
  Updates a transaction.

  ## Examples

      iex> update_transaction(transaction, %{field: new_value})
      {:ok, %Transaction{}}

      iex> update_transaction(transaction, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_transaction(%Transaction{} = transaction, attrs) do
    transaction
    |> Transaction.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Transaction.

  ## Examples

      iex> delete_transaction(transaction)
      {:ok, %Transaction{}}

      iex> delete_transaction(transaction)
      {:error, %Ecto.Changeset{}}

  """
  def delete_transaction(%Transaction{} = transaction) do
    Repo.delete(transaction)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking transaction changes.

  ## Examples

      iex> change_transaction(transaction)
      %Ecto.Changeset{source: %Transaction{}}

  """
  def change_transaction(%Transaction{} = transaction) do
    Transaction.changeset(transaction, %{})
  end

  def parse_csv(rows) do
    Enum.each(rows, fn row ->
      case row do
        {:ok, result} ->
          hold = create_transaction_from_csv(%{name: Enum.at(result, 0), amount: Enum.at(result, 1),
            account_id: Enum.at(result, 2), category_id: Enum.at(result, 3),
            desc: Enum.at(result, 4)})
        {:error, result} ->
          result
      end
    end)
  end

  def create_transaction_from_csv(transaction) do
      %Transaction{}
      |> Transaction.changeset(transaction)
      |> Repo.insert()
  end
end
