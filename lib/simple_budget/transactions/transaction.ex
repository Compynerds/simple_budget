defmodule SimpleBudget.Transactions.Transaction do
  use Ecto.Schema
  import Ecto.Changeset


  schema "transactions" do
    field :amount, :float
    field :desc, :string
    field :name, :string
    field :soft_delete, :boolean, default: false
    field :account_id, :integer
    field :category_id, :integer

    timestamps()
  end

  @doc false
  def changeset(transaction, attrs) do
    transaction
    |> cast(attrs, [:name, :amount, :desc, :soft_delete, :account_id, :category_id])
    |> validate_required([:name, :amount, :desc, :soft_delete, :account_id, :category_id])
  end
end
