defmodule SimpleBudget.Accounts.Account do
  use Ecto.Schema
  import Ecto.Changeset


  schema "accounts" do
    field :desc, :string
    field :name, :string
    field :number, :string
    field :soft_delete, :boolean, default: false
    field :total_amount, :float

    timestamps()
  end

  @doc false
  def changeset(account, attrs) do
    account
    |> cast(attrs, [:name, :number, :desc, :total_amount, :soft_delete])
    |> validate_required([:name, :number, :desc, :total_amount, :soft_delete])
    |> unique_constraint(:number)
  end
end
