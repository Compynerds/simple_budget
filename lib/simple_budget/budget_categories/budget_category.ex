defmodule SimpleBudget.BudgetCategories.BudgetCategory do
  use Ecto.Schema
  import Ecto.Changeset


  schema "budget_categories" do
    field :amount, :float
    field :desc, :string
    field :name, :string
    field :soft_delete, :boolean, default: false
    field :account_id, :integer

    timestamps()
  end

  @doc false
  def changeset(budget_category, attrs) do
    budget_category
    |> cast(attrs, [:name, :amount, :desc, :soft_delete, :account_id])
    |> validate_required([:name, :amount, :desc, :soft_delete, :account_id])
    |> unique_constraint(:name)
  end
end
