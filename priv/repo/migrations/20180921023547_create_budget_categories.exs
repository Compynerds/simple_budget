defmodule SimpleBudget.Repo.Migrations.CreateBudgetCategories do
  use Ecto.Migration

  def change do
    create table(:budget_categories) do
      add :name, :string
      add :amount, :float
      add :desc, :text
      add :account_id, :integer
      add :soft_delete, :boolean, default: false, null: false

      timestamps()
    end

    create unique_index(:budget_categories, [:name])
  end
end
