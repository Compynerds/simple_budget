defmodule SimpleBudget.Repo.Migrations.CreateAccounts do
  use Ecto.Migration

  def change do
    create table(:accounts) do
      add :name, :string
      add :number, :string
      add :desc, :text
      add :total_amount, :float
      add :soft_delete, :boolean, default: false, null: false

      timestamps()
    end

    create unique_index(:accounts, [:number])
  end
end
