defmodule SimpleBudget.Repo.Migrations.CreateTransactions do
  use Ecto.Migration

  def change do
    create table(:transactions) do
      add :name, :string
      add :amount, :float
      add :desc, :text
      add :account_id, :integer
      add :category_id, :integer
      add :soft_delete, :boolean, default: false, null: false

      timestamps()
    end

  end
end
